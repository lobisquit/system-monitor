import io
import logging
import os
import re
import subprocess
from subprocess import PIPE

from flask import Flask, flash, redirect, render_template, request, send_file
from flask_simplelogin import SimpleLogin, login_required
from werkzeug.security import check_password_hash

app = Flask(__name__)

# read parameters, fail if missing
app.config['SECRET_KEY'] = os.environ['SECRET_KEY']

UNITS = os.environ['UNITS'].split(',')
HASHED_USER_PASSWORD = os.environ['HASHED_USER_PASSWORD']

# setup logger
logger = logging.getLogger("scan")
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logger.setLevel(level=LOGLEVEL)


# setup the login manager
def login_checker(user):
    if user.get('username') == 'user' and \
       check_password_hash(HASHED_USER_PASSWORD, user.get('password')):
        return True
    return False


SimpleLogin(app, login_checker=login_checker)


@app.route("/")
@login_required
def main():
    entries = []

    # show basic top command output
    top_command = subprocess.run(['top', '-b', '-n', '1'], stdout=PIPE, stderr=PIPE)
    raw_top = top_command.stdout.decode("utf-8").split('\n')

    load = re.search(r'.*(load average: .*)', raw_top[0])
    memory = re.search(r'MiB Mem :\s*(\d*\.\d*) total,\s*(\d*\.\d*) free,\s*(\d*\.\d*) used', raw_top[3])

    entries.append({
        'raw': raw_top,
        'service': 'system',
        'active': load.group(1) if load else '...',
        'enabled': f'memory {memory.group(3)}MiB/{memory.group(1)}MiB' if memory else '...',
    })

    # handle requested systemd units
    activity_parser = re.compile(r'.*Active: (.*) since .*')
    enabled_parser = re.compile('.*Loaded: .* \([^;]*; ([a-z]*); [^;]*\)')

    for unit in UNITS:
        command = subprocess.run(['systemctl', 'status', unit], stdout=PIPE, stderr=PIPE)
        raw_status = command.stdout.decode("utf-8").split('\n')

        active = activity_parser.search(raw_status[2])
        enabled = enabled_parser.search(raw_status[1])

        entries.append({
            'raw': raw_status,
            'service': unit,
            'active': active.group(1) if active else '...',
            'enabled': enabled.group(1) if enabled else '...',
        })

    return render_template('home.html', entries=entries)


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5001)
